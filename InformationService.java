package com.example.ers.service;

import com.example.ers.model.Information;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface InformationService {

    List<Information> getAllInformation(Information information);

    PageInfo getInformationInformation(Information information, Integer pageNum);

    void updateInformation(Information information);

    void saveInformation(Information information);

    void deleteInformation(Information information);

    Integer selectInformationByName(Information information);

    void updateOrder(Integer order1, Integer order2);
}
