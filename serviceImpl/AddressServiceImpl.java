package com.example.ers.service.serviceImpl;

import com.example.ers.dao.AddressDao;
import com.example.ers.model.Address;
import com.example.ers.service.AddressService;
import com.example.ers.util.MyConstant;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {
    @Autowired
    private AddressDao addressDao;

    @Override
    public PageInfo getAddressInformation(Address address, Integer pageNum) {
        PageHelper.startPage(pageNum, MyConstant.PAGE_SIZE);
        PageInfo pageInfo = new PageInfo(addressDao.getAddressInformation(address));
        return pageInfo;
    }

    @Override
    public void updateAddress(Address address) {
        addressDao.updateAddress(address);
    }

    @Override
    public void saveAddress(Address address) {
        addressDao.saveAddress(address);
    }

    @Override
    public void deleteAddress(Address address) {
        addressDao.deleteAddress(address);
    }

    @Override
    public Integer selectAddressByName(Address address) {
        return addressDao.selectAddressByName(address);
    }

    @Override
    public List<Address> getAllAddress(Address address) {
        return addressDao.getAddressInformation(address);
    }
}
