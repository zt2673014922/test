package com.example.ers.service.serviceImpl;

import com.example.ers.dao.CompanyDao;
import com.example.ers.model.Company;
import com.example.ers.service.CompanyService;
import com.example.ers.util.MyConstant;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    private CompanyDao companyDao;


    @Override
    public Object getCompanyInformation(Company company, Integer pageNum) {
        PageHelper.startPage(pageNum, MyConstant.PAGE_SIZE);
        List<Company> companies = companyDao.getCompanyInformation(company);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
        for (int i = 0; i < companies.size(); i++) {
                String sd = simpleDateFormat.format(new Date(companies.get(i).getCreateTime()));
                companies.get(i).setDate(sd);
        }
        PageInfo pageInfo = new PageInfo(companies);
        return pageInfo;
    }

    @Override
    public void updateCompany(Company company) {
        companyDao.updateCompany(company);
    }

    @Override
    public void blockCompany(Company company) {
        companyDao.blockCompany(company);
    }

    @Override
    public void deleteCompany(Company company) {
        companyDao.deleteCompany(company);
    }

    @Override
    public Integer selectCompanyByName(Company company) {
        return companyDao.selectCompanyByName(company);
    }

    @Override
    public List<Company> getAllCompany(Company company) {
        return companyDao.getCompanyInformation(company);
    }
}
