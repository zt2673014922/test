package com.example.ers.service.serviceImpl;

import com.example.ers.dao.RoleDao;
import com.example.ers.model.Role;
import com.example.ers.service.RoleService;
import com.example.ers.util.MyConstant;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleDao roleDao;


    @Override
    public PageInfo getRoleInformation(Role role, Integer pageNum) {
        PageHelper.startPage(pageNum, MyConstant.PAGE_SIZE);
        return new PageInfo(roleDao.getRoleInformation(role));
    }

    @Override
    public void saveRole(Role role) {
        roleDao.saveRole(role);
    }

    @Override
    public void deleteRole(Role role) {
        roleDao.deleteRole(role);
    }

    @Override
    public void updateRole(Role role) {
        roleDao.updateRole(role);
    }

    @Override
    public List<Role> getAllRole(Role role) {
        return roleDao.getRoleInformation(role);
    }

    @Override
    public Integer selectRoleByName(Role role) {
        return roleDao.selectRoleByName(role);
    }
}
