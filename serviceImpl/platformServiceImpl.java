package com.example.ers.service.serviceImpl;

import com.example.ers.dao.PlatformDao;
import com.example.ers.model.Platform;
import com.example.ers.service.platformService;
import com.example.ers.util.MyConstant;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class platformServiceImpl implements platformService {
    @Autowired
    private PlatformDao platformDao;

    @Override
    public PageInfo getPlatformInformation(Platform platform, Integer pageNum) {
        PageHelper.startPage(pageNum, MyConstant.PAGE_SIZE);
        PageInfo pageInfo = new PageInfo(platformDao.getPlatformInformation(platform));
        return pageInfo;
    }

    @Override
    public Integer getPlatformByName(String name) {
        return platformDao.getPlatformByName(name);
    }

    @Override
    public void savePlatform(Platform platform) {
        platform.setPassword(DigestUtils.md5Hex(platform.getPassword()));
        platformDao.savePlatform(platform);
    }

    @Override
    public void deletePlatform(Platform platform) {
        platformDao.deletePlatform(platform);
    }

    @Override
    public void updatePlatform(Platform platform) {
        platform.setPassword(DigestUtils.md5Hex(platform.getPassword()));
        platformDao.updatePlatform(platform);
    }

    @Override
    public Platform login(Platform platform) {
        platform.setPassword(DigestUtils.md5Hex(platform.getPassword()));
        return platformDao.login(platform);
    }
}
