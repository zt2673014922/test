package com.example.ers.service.serviceImpl;

import com.example.ers.dao.CategoryDao;
import com.example.ers.model.Category;
import com.example.ers.service.CategoryService;
import com.example.ers.util.MyConstant;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryDao categoryDao;

    @Override
    public List<Category> getAllCategory(Category category) {
        return categoryDao.getCategoryInformation(category);
    }

    @Override
    public PageInfo getCategoryInformation(Category category, Integer pageNum) {
        PageHelper.startPage(pageNum, MyConstant.PAGE_SIZE);
        PageInfo pageInfo = new PageInfo(categoryDao.getCategoryInformation(category));
        return pageInfo;
    }

    @Override
    public void saveCategory(Category category) {
        categoryDao.saveCategory(category);
    }

    @Override
    public void deleteCategory(Category category) {
        categoryDao.deleteCategory(category);
    }

    @Override
    public void updateCategory(Category category) {
        categoryDao.updateCategory(category);
    }

    @Override
    public Integer getCategoryByName(Category category) {
        return categoryDao.getCategoryByName(category);
    }
}
