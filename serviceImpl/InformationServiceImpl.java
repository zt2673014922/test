package com.example.ers.service.serviceImpl;

import com.example.ers.dao.InformationDao;
import com.example.ers.model.Information;
import com.example.ers.service.InformationService;
import com.example.ers.util.MyConstant;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
@Service
public class InformationServiceImpl implements InformationService {
    @Autowired
    private InformationDao informationDao;


    @Override
    public List<Information> getAllInformation(Information information) {
        return informationDao.getAllInformation(information);
    }

    @Override
    public PageInfo getInformationInformation(Information information, Integer pageNum) {
        PageHelper.startPage(pageNum, MyConstant.PAGE_SIZE);
        PageInfo pageInfo = new PageInfo(informationDao.getAllInformation(information));
        return pageInfo;
    }

    @Override
    public void updateInformation(Information information) {
        informationDao.updateInformation(information);
    }

    @Override
    public void saveInformation(Information information) {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String s = simpleDateFormat.format(date);
        information.setTime(s);
        informationDao.saveInformation(information);
    }

    @Override
    public void deleteInformation(Information information) {
        informationDao.deleteInformation(information);
    }

    @Override
    public Integer selectInformationByName(Information information) {
        return informationDao.selectInformationByName(information);
    }

    @Override
    public void updateOrder(Integer order1, Integer order2) {
        informationDao.updateOrder(order1, order2);
    }
}
