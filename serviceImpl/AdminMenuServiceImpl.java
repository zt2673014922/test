package com.example.ers.service.serviceImpl;

import com.example.ers.dao.AdminMenuDao;
import com.example.ers.model.Menu;
import com.example.ers.model.TreeData;
import com.example.ers.model.TreeNode;
import com.example.ers.service.AdminMenuService;
import com.example.ers.util.MyConstant;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminMenuServiceImpl implements AdminMenuService {
    @Autowired
    private AdminMenuDao adminMenuDao;

    @Override
    public List<Menu> getMenuByRole(Integer roleId) {
        return adminMenuDao.getMenuByRole(roleId);
    }
    @Override
    public TreeData listMenusWithRole(Integer roleId) {
        List<TreeNode> data = adminMenuDao.listTreeNode();
        List<Integer> defaultExpandedKeys = adminMenuDao.listDefaultExpandedKeys(roleId);
        List<Integer> defaultCheckedKeys = adminMenuDao.listDefaultCheckedKeys(roleId);
        TreeData treeData = new TreeData();
        treeData.setData(data);
        treeData.setDefaultCheckedKeys(defaultCheckedKeys);
        treeData.setDefaultExpandedKeys(defaultExpandedKeys);
        return treeData;
    }

    @Override
    public void updateRoleMenu(Integer roleId, Integer[] menuIds) {
        adminMenuDao.deleteRoleMenu(roleId);
        adminMenuDao.updateRoleMenu(roleId, menuIds);
    }

    @Override
    public List<Menu> getAllMenu(Menu menu) {
        return adminMenuDao.getMenuInformation(menu);
    }

    @Override
    public PageInfo getMenuInformation(Menu menu, Integer pageNum) {
        PageHelper.startPage(pageNum, MyConstant.PAGE_SIZE);
        PageInfo pageInfo = new PageInfo(adminMenuDao.getMenuInformation(menu));
        return pageInfo;
    }

    @Override
    public void saveMenu(Menu menu) {
        menu.setStatus(1);
        adminMenuDao.saveMenu(menu);
    }

    @Override
    public void deleteMenu(Menu menu) {
        adminMenuDao.deleteMenu(menu);
    }

    @Override
    public void updateMenu(Menu menu) {
        adminMenuDao.updateMenu(menu);
    }

    @Override
    public Integer selectMenuByName(Menu menu) {
        return adminMenuDao.selectMenuByName(menu);
    }
}
