package com.example.ers.service.serviceImpl;

import com.example.ers.dao.WelfareDao;
import com.example.ers.model.Welfare;
import com.example.ers.service.WelfareService;
import com.example.ers.util.MyConstant;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WelfareServiceImpl implements WelfareService {
    @Autowired
    private WelfareDao welfareDao;

    @Override
    public PageInfo getWelfareInformation(Welfare welfare,Integer pageNum) {
        PageHelper.startPage(pageNum, MyConstant.PAGE_SIZE);
        PageInfo pageInfo = new PageInfo(welfareDao.getWelfareInformation(welfare));
        return pageInfo;
    }

    @Override
    public void saveWelfare(Welfare welfare) {
        welfareDao.saveWelfare(welfare);
    }

    @Override
    public void deleteWelfare(Welfare welfare) {
        welfareDao.deleteWelfare(welfare);
    }

    @Override
    public void updateWelfare(Welfare welfare) {
        welfareDao.updateWelfare(welfare);
    }

    @Override
    public Integer getWelfareByContent(Welfare welfare) {
        return welfareDao.getWelfareByContent(welfare);
    }

    @Override
    public List<Welfare> getAllWelfare(Welfare welfare) {
        return welfareDao.getWelfareInformation(welfare);
    }
}
