package com.example.ers.service.serviceImpl;

import com.example.ers.dao.AdsDao;
import com.example.ers.model.Ads;
import com.example.ers.service.AdsService;
import com.example.ers.util.MyConstant;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdsServiceImpl implements AdsService {
    @Autowired
    private AdsDao adsDao;

    @Override
    public PageInfo getAdsInformation(Ads ads, Integer pageNum) {
        PageHelper.startPage(pageNum, MyConstant.PAGE_SIZE);
        PageInfo pageInfo = new PageInfo(adsDao.getAdsInformation(ads));
        return pageInfo;
    }

    @Override
    public void saveAds(Ads ads) {
        adsDao.saveAds(ads);
    }

    @Override
    public void deleteAds(Ads ads) {
        adsDao.deleteAds(ads);
    }

    @Override
    public void updateAds(Ads ads) {
        adsDao.updateAds(ads);
    }


    @Override
    public List<Ads> getAllAds(Ads ads) {
        return adsDao.getAdsInformation(ads);
    }

    @Override
    public void updatePriority(Integer originalPriority ,Integer priority) {
        adsDao.updatePriority(originalPriority,priority);
    }
}
