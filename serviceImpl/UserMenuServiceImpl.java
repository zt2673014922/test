package com.example.ers.service.serviceImpl;

import com.example.ers.dao.UserMenuDao;
import com.example.ers.model.Menu;
import com.example.ers.service.UserMenuService;
import com.example.ers.util.MyConstant;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserMenuServiceImpl implements UserMenuService {
    @Autowired
    private UserMenuDao userMenuDao;


    @Override
    public List<Menu> getAllMenu(Menu menu) {
        return userMenuDao.getMenuInformation(menu);
    }

    @Override
    public PageInfo getMenuInformation(Menu menu, Integer pageNum) {
        PageHelper.startPage(pageNum, MyConstant.PAGE_SIZE);
        PageInfo pageInfo = new PageInfo(userMenuDao.getMenuInformation(menu));
        return pageInfo;
    }

    @Override
    public void saveMenu(Menu menu) {
        menu.setStatus(2);
        userMenuDao.saveMenu(menu);
    }

    @Override
    public void deleteMenu(Menu menu) {
        userMenuDao.deleteMenu(menu);
    }

    @Override
    public void updateMenu(Menu menu) {
        userMenuDao.updateMenu(menu);
    }

    @Override
    public Integer selectMenuByName(Menu menu) {
       return userMenuDao.selectMenuByName(menu);
    }
}
