package com.example.ers.service;

import com.example.ers.model.Role;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface RoleService {
    PageInfo getRoleInformation(Role role, Integer pageNum);

    void saveRole(Role role);

    void deleteRole(Role role);

    void updateRole(Role role);

    List<Role> getAllRole(Role role);

    Integer selectRoleByName(Role role);
}
