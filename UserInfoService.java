package com.example.ers.service;

import com.example.ers.model.UserInfo;
import com.github.pagehelper.PageInfo;

public interface UserInfoService {
    PageInfo getUserInfoInformation(UserInfo userInfo, Integer pageNum);

    void updateUserInfo(UserInfo userInfo);

    void deleteUserInfo(UserInfo userInfo);
}
