package com.example.ers.service;

import com.example.ers.model.Address;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface AddressService {
    PageInfo getAddressInformation(Address address, Integer pageNum);

    void updateAddress(Address address);

    void saveAddress(Address address);

    void deleteAddress(Address address);

    Integer selectAddressByName(Address address);

    List<Address> getAllAddress(Address address);
}
