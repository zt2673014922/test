package com.example.ers.service;


import com.example.ers.model.Menu;
import com.example.ers.model.TreeData;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface AdminMenuService {
    List<Menu> getMenuByRole(Integer roleId);

    TreeData listMenusWithRole(Integer rid);

    void updateRoleMenu(Integer rid, Integer[] menuIds);

    List<Menu> getAllMenu(Menu menu);

    PageInfo getMenuInformation(Menu menu, Integer pageNum);

    void saveMenu(Menu menu);

    void deleteMenu(Menu menu);

    void updateMenu(Menu menu);

    Integer selectMenuByName(Menu menu);
}
