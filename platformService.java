package com.example.ers.service;

import com.example.ers.model.Platform;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface platformService {

     PageInfo getPlatformInformation(Platform platform, Integer pageNum);
     
     Integer getPlatformByName(String name);//查重复用户名

     void savePlatform(Platform platform);

     void deletePlatform(Platform platform);

     void updatePlatform(Platform platform);

    Platform login(Platform platform);
}
