package com.example.ers.service;

import com.example.ers.model.Company;

import java.util.List;

public interface CompanyService {
    Object getCompanyInformation(Company company, Integer pageNum);

    void updateCompany(Company company);

    void blockCompany(Company company);

    void deleteCompany(Company company);

    Integer selectCompanyByName(Company company);

    List<Company> getAllCompany(Company company);
}
