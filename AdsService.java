package com.example.ers.service;

import com.example.ers.model.Ads;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface AdsService {
    PageInfo getAdsInformation(Ads ads, Integer pageNum);

    void saveAds(Ads ads);

    void deleteAds(Ads ads);

    void updateAds(Ads ads);


    List<Ads> getAllAds(Ads ads);

    void updatePriority(Integer originalPriority ,Integer priority);
}
