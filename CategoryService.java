package com.example.ers.service;

import com.example.ers.model.Category;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategory(Category category);

    PageInfo getCategoryInformation(Category category, Integer pageNum);

    void saveCategory(Category category);

    void deleteCategory(Category category);

    void updateCategory(Category category);

    Integer getCategoryByName(Category category);
}
